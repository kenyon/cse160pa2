import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;

/**
 * K-means clustering program, sequential version. See
 * assignment/cse160pa2-assignment.html for details. Usage:
 * <code>java KMeansSeq <K> <inputfile> <outputfile></code> where <K> is the
 * number of clusters (K at least 1), <inputfile> is the input file name, and
 * <outputfile> is the output file name.
 * 
 * @author Kenyon Ralph <kenyon@kenyonralph.com>
 */
public class KMeansSeq {
    /**
     * Class to encapsulate a data point.
     * 
     * @author Kenyon Ralph <kenyon@kenyonralph.com>
     */
    public static class Point implements Cloneable {
	/**
	 * Coordinate values of this point.
	 */
	private double[] coordinates;

	/**
	 * The cluster this data point belongs to. Unassigned points have a
	 * cluster value of -1. Clusters are numbered 0, 1, 2, ..., k-1, where k
	 * is the number of clusters given on the command line.
	 */
	private int      cluster;

	/**
	 * Creates a new point with the given coordinate values, given as an
	 * array of double-precision coordinates. Initially, the data point is
	 * not assigned to any cluster.
	 * 
	 * @param coords the coordinates of this point
	 */
	Point(double[] coords) {
	    this.setCoordinates(coords);
	    this.setCluster(-1);
	}

	/**
	 * Sets the coordinates of this point.
	 * 
	 * @param coords the coordinates to set for this point
	 */
	private void setCoordinates(double[] coords) {
	    this.coordinates = coords;
	}

	/**
	 * Returns the coordinates of this point.
	 * 
	 * @return coordinates of this point
	 */
	public double[] getCoordinates() {
	    return this.coordinates;
	}

	/**
	 * Returns the cluster this point is assigned to.
	 * 
	 * @return the cluster this point is assigned to
	 */
	public int getCluster() {
	    return this.cluster;
	}

	/**
	 * Assigns this point to the given cluster.
	 * 
	 * @param c the cluster to assign this data point to. -1 for unassigned,
	 *            otherwise cluster numbers are 0, 1, ..., k-1, where k is
	 *            the number of clusters given on the command line.
	 */
	public void setCluster(int c) {
	    this.cluster = c;
	}

	/**
	 * Returns the Euclidean distance from this Point to the given Point.
	 * 
	 * @param p the Point from which to find the distance to this Point
	 * @return double-precision value of the Euclidean distance from this
	 *         Point to the given Point
	 */
	public double distance(Point p) {
	    /*
	     * The sum of the coordinate differences (the part under the square
	     * root in the Euclidean distance formula).
	     */
	    double sum = 0;

	    for (int i = 0; i < this.getCoordinates().length; ++i) {
		sum += Math.pow(this.getCoordinates()[i] - p.getCoordinates()[i], 2);
	    }

	    return Math.sqrt(sum);
	}

	/**
	 * Returns a single-line string representation of this Point.
	 * 
	 * @return a string representation of this Point.
	 */
	@SuppressWarnings("boxing")
	@Override
	public String toString() {
	    StringBuilder s = new StringBuilder();

	    s.append("(");
	    for (int i = 0; i < this.getCoordinates().length; ++i) {
		s.append(String.format("%06.6f", this.getCoordinates()[i]));
		if (i + 1 != this.getCoordinates().length) {
		    s.append(",\t");
		}
	    }
	    s.append(")");
	    if (this.getCluster() != -1) {
		s.append("\t" + "cluster: " + this.getCluster());
	    }

	    return s.toString();
	}

	/**
	 * Creates and returns a copy of this Point.
	 * 
	 * @return a copy of this Point
	 */
	@Override
	public Object clone() {
	    Point copy;

	    try {
		copy = (Point) super.clone();
		return copy;
	    } catch (CloneNotSupportedException e) {
		// This should never happen, since Point extends Object, which
		// does indeed support cloning.
		return null;
	    }
	}
    }

    /**
     * Normal exit status.
     */
    private static final int       EXIT_SUCCESS   = 0;

    /**
     * Abnormal exit status, in case the command line arguments have errors.
     */
    private static final int       EXIT_USAGE     = 1;

    /**
     * Abnormal exit status, in case of a malformed input file.
     */
    private static final int       EXIT_BADINPUT  = 2;

    /**
     * Abnormal exit status, in case of an I/O exception while writing the
     * output file.
     */
    private static final int       EXIT_BADOUTPUT = 3;

    /**
     * The input file stream.
     */
    private static DataInputStream dis;

    /**
     * The output file writer.
     */
    private static FileWriter      writer;

    /**
     * The input file name.
     */
    private static String	  inputFileName;

    /**
     * The output file name.
     */
    private static String	  outputFileName;

    /**
     * The number of clusters.
     */
    private static int	     k;

    /**
     * The number of dimensions.
     */
    private static int	     d;

    /**
     * The number of data points.
     */
    private static int	     n;

    /**
     * The data points. There will be n elements in this array.
     */
    private static Point[]	 points;

    /**
     * The coordinates of the centers of the clusters. This is a k-element array
     * of Points.
     */
    private static Point[]	 centers;

    /**
     * Whether a data point changed clusters during point-to-cluster
     * assignments.
     */
    private static boolean	 moved	  = true;

    /**
     * Sets the center of the given cluster to the given point.
     * 
     * @param cluster the cluster to recenter
     * @param newCenter the point to set as the new center for the give cluster
     */
    private static void setClusterCenter(int cluster, Point newCenter) {
	centers[cluster] = newCenter;
    }

    /**
     * Returns the point at the center of the given cluster.
     * 
     * @param cluster the cluster whose center point is wanted
     * @return the center point of the given cluster
     */
    private static Point getCenterOfCluster(int cluster) {
	return centers[cluster];
    }

    /**
     * Gives the number of data points in the given cluster.
     * 
     * @param cluster the cluster in which to count the number of data points
     * @return the number of data points in the given cluster
     */
    private static int pointsInCluster(int cluster) {
	int count = 0;

	for (Point p : points) {
	    if (p.getCluster() == cluster) {
		++count;
	    }
	}

	return count;
    }

    /**
     * Returns a string representation of a point array. One point per line.
     * 
     * @param a the array of points to print
     * @return string representation of a point coordinate array
     */
    private static String pointArrayToString(Point[] a) {
	StringBuilder s = new StringBuilder();

	for (Point p : a) {
	    s.append(p.toString());
	    s.append("\n");
	}

	return s.toString();
    }

    /**
     * Returns the input file in plain text as a string. Useful for verifying
     * that the input data file is being read correctly.
     * 
     * @return string representation of the input file
     */
    @SuppressWarnings("unused")
    private static String inputFileToString() {
	StringBuilder s = new StringBuilder();
	s.append(d);
	s.append("\n");
	s.append(n);
	s.append("\n");
	s.append(pointArrayToString(points));
	return s.toString();
    }

    /**
     * Prints usage information to standard output.
     */
    private static void printUsage() {
	System.out.println("Usage: java KMeansSeq <k> <inputfile> <outputfile>");
	System.out.println("k:		integer number of clusters (at least 1)");
	System.out.println("inputfile:	input file name");
	System.out.println("outputfile:	output file name");
    }

    /**
     * Returns true if the arguments are valid, false otherwise. If an error
     * exists in the command line arguments, an appropriate message is printed
     * to standard error.
     * 
     * @param args command line arguments
     * @return true if the arguments are valid, false otherwise
     */
    private static boolean validArgs(String[] args) {
	if (args.length != 3) {
	    System.err.println("Error: must have 3 arguments.");
	    return false;
	}

	try {
	    k = Integer.parseInt(args[0]);
	} catch (NumberFormatException e) {
	    System.err.println("Error: k must be an integer.");
	    return false;
	}

	if (k <= 0) {
	    System.err.println("Error: k must be at least 1.");
	    return false;
	}

	inputFileName = args[1];
	outputFileName = args[2];

	try {
	    dis = new DataInputStream(new FileInputStream(inputFileName));
	} catch (FileNotFoundException e) {
	    System.err.println("Error: input file not found or cannot be read.");
	    return false;
	}

	try {
	    writer = new FileWriter(outputFileName);
	} catch (IOException e) {
	    System.err.println("Error: output file cannot be opened.");
	    return false;
	}

	return true;
    }

    /**
     * Prints an error message and exits in case of a badly-formatted input
     * file.
     */
    private static void malformedInput() {
	System.err.println("Error: malformed input file.");
	exit(EXIT_BADINPUT);
    }

    /**
     * Closes input and output streams.
     */
    private static void cleanup() {
	if (dis != null) {
	    try {
		dis.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}

	if (writer != null) {
	    try {
		writer.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }

    /**
     * Exits the program with the given exit status. This runs cleanup(), so
     * this method helps to never forget to run cleanup() before exiting.
     * 
     * @param status the exit status to return to the operating system
     */
    private static void exit(int status) {
	cleanup();
	System.exit(status);
    }

    /**
     * Reads the number of dimensions from the input file.
     */
    private static void readDimensions() {
	/*
	 * I thought these try/catch blocks would detect malformed input files,
	 * but generating random input files shows that they don't. Instead the
	 * JVM seems to throw random exceptions in readPoints().
	 */
	try {
	    d = dis.readInt();
	} catch (IOException e) {
	    malformedInput();
	}
    }

    /**
     * Reads the number of points from the input file.
     */
    private static void readNumberOfPoints() {
	try {
	    n = dis.readInt();
	} catch (IOException e) {
	    malformedInput();
	}
    }

    /**
     * Reads points from the input file into the points array.
     */
    private static void readPoints() {
	points = new Point[n];
	Point p;
	double[] coords;

	for (int i = 0; i < points.length; ++i) {
	    coords = new double[d];
	    for (int j = 0; j < d; ++j) {
		try {
		    coords[j] = dis.readDouble();
		} catch (IOException e) {
		    malformedInput();
		}
	    }
	    p = new Point(coords);
	    points[i] = p;
	}
    }

    /**
     * Initializes the k cluster centers to be the first k data points.
     */
    private static void initializeCenters() {
	centers = new Point[k];
	for (int i = 0; i < k; ++i) {
	    centers[i] = (Point) points[i].clone();
	}
    }

    /**
     * Returns the integer cluster number whose center is closest to the given
     * data point, using the Euclidean distance.
     * 
     * @param p the data point which we want to find the nearest cluster to
     * @return the cluster number whose center is closest to the given data
     *         point
     */
    @SuppressWarnings("boxing")
    private static int nearestCluster(Point p) {
	/*
	 * Mapping of distances as keys to clusters as values. The cluster
	 * values are integers corresponding to their index in centers[].
	 */
	TreeMap<Double, Integer> distancesFromClusterCenters = new TreeMap<Double, Integer>();

	for (int cluster = 0; cluster < k; ++cluster) {
	    distancesFromClusterCenters.put(getCenterOfCluster(cluster).distance(p), cluster);
	}

	return ((Integer) distancesFromClusterCenters.values().toArray()[0]).intValue();
    }

    /**
     * Assigns all points to the cluster whose center is closest using the
     * Euclidean distance.
     * 
     * @return true if any point changed clusters during this assignment process
     */
    private static boolean assignPointsToNearestCluster() {
	int previousCluster;
	boolean moved_local = false; // Whether a point switched clusters.

	for (Point p : points) {
	    previousCluster = p.getCluster();
	    p.setCluster(nearestCluster(p));
	    if (!moved_local && p.getCluster() != previousCluster) {
		moved_local = true;
	    }
	}

	return moved_local;
    }

    /**
     * Updates the values of the cluster centers. The cluster center is defined
     * to be the mean of all the data points in the cluster. Specifically, each
     * coordinate of the cluster center is the mean of the corresponding
     * coordinates of the data points in the cluster.
     */
    private static void updateClusterCenters() {
	/*
	 * sums[i] is the sum of the ith dimension of all the points in this
	 * cluster.
	 */
	double[] sums = new double[d];
	double[] newCenterCoordinates;

	for (int cluster = 0; cluster < k; ++cluster) {
	    for (int i = 0; i < sums.length; ++i) {
		sums[i] = 0;
	    }

	    for (Point p : points) {
		if (cluster == p.getCluster()) {
		    for (int dimension = 0; dimension < d; ++dimension) {
			sums[dimension] += p.getCoordinates()[dimension];
		    }
		}
	    }

	    newCenterCoordinates = new double[d];
	    for (int dimension = 0; dimension < d; ++dimension) {
		newCenterCoordinates[dimension] = sums[dimension] / pointsInCluster(cluster);
	    }
	    setClusterCenter(cluster, new Point(newCenterCoordinates));
	}
    }

    /**
     * Writes the cluster centers to the output file in the format specified by
     * the assignment.
     */
    private static void writeOutputFile() {
	try {
	    for (Point p : centers) {
		for (int dimension = 0; dimension < d; ++dimension) {
		    writer.write(p.getCoordinates()[dimension] + "");
		    if (dimension + 1 != d) {
			writer.write("\t");
		    }
		}
		writer.write("\n");
	    }
	} catch (IOException e) {
	    System.err.println("Error: I/O error while writing to output file.");
	    exit(EXIT_BADOUTPUT);
	}
    }

    /**
     * The main k-means clustering program.
     * 
     * @param args command line arguments: <K> <inputfile> <outputfile>
     */
    public static void main(String[] args) {
	if (!validArgs(args)) {
	    printUsage();
	    exit(EXIT_USAGE);
	}

	long startTime = -System.currentTimeMillis();

	readDimensions();
	readNumberOfPoints();
	readPoints();
	initializeCenters();

	do {
	    moved = assignPointsToNearestCluster();
	    updateClusterCenters();
	} while (moved);

	writeOutputFile();

	System.out.println(startTime + System.currentTimeMillis());
	exit(EXIT_SUCCESS);
    }
}
